//
//  StockFeedUITests.swift
//  StockFeedUITests
//
//  Created by Deepak Kumar on 13/04/21.
//

import XCTest

class StockFeedUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testHome() throws {
        
        let app = XCUIApplication()
        snapshot("SignupScreen")
        let enterNameTextField = app.textFields["Enter Name"]
        enterNameTextField.tap()
        
        let enterEmailTextField = app.textFields["Enter Email"]
        enterEmailTextField.tap()
        enterNameTextField.tap()
        enterNameTextField.tap()
        enterEmailTextField.tap()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.tap()
        app.secureTextFields["Enter Password"].tap()
        enterNameTextField.tap()
        snapshot("Signup1Screen")
        app.buttons["Sign Up"].tap()
        snapshot("HomeScreen")
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["May 4, 2021 at 1:57:06 PM"]/*[[".cells[\"AAPL, 132.11, May 4, 2021 at 1:57:06 PM\"].staticTexts[\"May 4, 2021 at 1:57:06 PM\"]",".staticTexts[\"May 4, 2021 at 1:57:06 PM\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery.cells["GOGL, 9.37, May 4, 2021 at 1:57:13 PM"].otherElements.containing(.staticText, identifier:"GOGL").element.tap()
        snapshot("HomeScreen1")
        XCUIApplication().buttons["Login"].tap()
        snapshot("01LoginScreen")
                
    }
    
    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
