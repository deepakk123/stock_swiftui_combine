//
//  StockFeedApp.swift
//  StockFeed
//
//  Created by Deepak Kumar on 13/04/21.
//

import SwiftUI

@main
struct StockFeedApp: App {
    @StateObject private var modelData = ModelData()
    
    var body: some Scene {
        WindowGroup {
            //ContentView()
            LandmarkContentView()
                .environmentObject(modelData)
        }
    }
}
