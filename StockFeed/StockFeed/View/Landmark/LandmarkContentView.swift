//
//  LandmarkContentView.swift
//  StockFeed
//
//  Created by Deepak Kumar on 21/06/21.
//

import SwiftUI

struct LandmarkContentView: View {
    @State private var selection: Tab = .featured
    
    enum Tab {
        case featured
        case list
    }
    
    var body: some View {
        TabView(selection: $selection) {
            CategoryHome()
                .tabItem {
                    Label("Featured", systemImage: "star")
                }
                .tag(Tab.featured)
            
            LandmarkList()
                .tabItem {
                    Label("List", systemImage: "list.bullet")
                }
                .tag(Tab.list)
        }
    }
}

struct LandmarkContentView_Previews: PreviewProvider {
    static var previews: some View {
        LandmarkContentView()
            .environmentObject(ModelData())
    }
}
