//
//  LandmarkDetail.swift
//  StockFeed
//
//  Created by Deepak Kumar on 18/06/21.
//

import SwiftUI

struct LandmarkDetail: View {
    @EnvironmentObject var modelData: ModelData
    var landmark: Landmark
    
    var landmarkIndex: Int {
        modelData.landmarks.firstIndex(where: { $0.id == landmark.id })!
    }
    
    var body: some View {
        ScrollView {
            MapView(coordinate: landmark.locationCoordinate)
                .ignoresSafeArea(edges: .top)
                .frame(height: 300)
            
            CircleImage(image: landmark.image)
                .offset(y: -100)
                .padding(.bottom, -100)
            
            VStack(alignment: .leading) {
                HStack {
                    Text(landmark.name)
                        .font(.title)
                        .fontWeight(.regular)
                        .foregroundColor(.blue)
                    
                    FavoriteButton(isSet: $modelData.landmarks[landmarkIndex].isFavorite)
                }
                
                HStack {
                    Text(landmark.park)
                        
                    Spacer()
                    
                    Text(landmark.state)
                }.font(.subheadline)
                .foregroundColor(.secondary)        //Will apply to all elemets inside stack.
                
                Divider()
                
                Text("About \(landmark.name)")
                    .font(.title)
                Text(landmark.description)
                
            }
            .padding()
        }
        .navigationTitle(landmark.name)
        //.navigationBarTitleDisplayMode(.inline)
    }
}

struct LandmarkDetail_Previews: PreviewProvider {
    static var previews: some View {
        LandmarkDetail(landmark: ModelData().landmarks[0])
    }
}
